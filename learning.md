# навчання

## Languages
### Chinese
 - [Китайский язык для начинающих: основы фонетики](https://openedu.ru/course/spbu/CNPHONETICS/)
 - [Китайский язык для начинающих](https://openedu.ru/course/spbu/CHINB/)
 - [Китайский язык для начинающих. Часть 2.](https://openedu.ru/course/spbu/CHINB2/)
 - [Введение в китайскую иероглифику](https://openedu.ru/course/spbu/HANZI/)
 - [КИТАЙСКИЙ ЯЗЫК. Уровень 1.](https://stepik.org/course/63098/promo?search=730248566)
 - [КИТАЙСКИЙ ЯЗЫК. Уровень 2.](https://stepik.org/course/70831/promo?search=730248570)
 - [КИТАЙСКИЙ ЯЗЫК. Уровень 3. Часть 1.](https://stepik.org/course/83843/promo?search=730248565)
 - [Китайский язык](https://stepik.org/course/1655/syllabus)
 - [Китайский язык для чайников (HSK 1)](https://stepik.org/course/49169/syllabus)
 - [Лексика из HSK 1 (китайский язык)](https://stepik.org/course/61032/promo?search=730248629)
### Arabian
 - [Арабский язык. Вводный курс](https://openedu.ru/course/spbu/ARBLNG/)
 - [Арабский язык. Часть 2](https://openedu.ru/course/spbu/ARBLNG2/)
### English
 - [Теоретическая грамматика английского языка](https://openedu.ru/course/spbu/TEORGRAM/)
 - [Английский язык для инженеров](https://openedu.ru/course/misis/ENG/)
 - [Математический английский](https://openedu.ru/course/hse/MATHENG/)
### Polish
 - [www.polskijazyk.pl](http://www.polskijazyk.pl/pl/e-szkolenia/moje-kursy/)
 - [Польский язык. Основы (А1). Быстрое обучение чтению](https://stepik.org/course/24913/syllabus)
 - [Польский язык. Часть 2 (А2). Czas na czasownik](https://stepik.org/course/36403/promo?search=730248595)
### German
 - [https://studway.com.ua/online-kursi-nimecka/](https://studway.com.ua/online-kursi-nimecka/)
 - [Немецкий с нуля](https://stepik.org/course/55553/promo?search=730248594)
 - [Грамматика немецкого языка для медиков и фармацевтов. Морфология](https://stepik.org/course/57742/promo?search=730248642)
### Japaneese
 - [Японский язык. Начальный уровень](https://openedu.ru/course/spbu/JPLANG/)
### Latin
 - [Латинский язык. Начальный курс](https://openedu.ru/course/spbu/LATLNG/)
 - [Латинский язык: Грамматический конструктор](https://stepik.org/course/1564/promo?search=730248571)
 - [Латинский язык (Основы фармацевтической терминологии)](https://stepik.org/course/72552/promo?search=730248632)
### Persian
 - [Персидский язык. Начальный разговорный курс](https://openedu.ru/course/spbu/PERS/)
### Koreyan
 - [Корейский язык. Вводный курс](https://stepik.org/course/91461/promo)
 - [Чтение и письмо на корейском](https://stepik.org/course/11291/promo)
 - [Корейский язык. Начальный разговорный курс](https://openedu.ru/course/spbu/KORLANG/)
### Turkish
 - [Турецкий язык. Начальный разговорный курс](https://openedu.ru/course/spbu/TURK/)
### Chech
 - [Как выучить чешский язык своими силами?](https://stepik.org/course/58369/promo?search=730248580)
### French
 - [Французский язык: Грамматический конструктор](https://stepik.org/course/1565/promo?search=730248589)
### Greek
 - [Древнегреческий язык: Грамматический конструктор](https://stepik.org/course/65025/promo?search=730248614)
### Spanish
 - [Введение в грамматику испанского языка](https://stepik.org/course/50224/promo?search=730248627)
### Slovak
 - [slovake.eu](https://slovake.eu/ru/courses/a1)
## AI
- [Машинное обучение](https://openedu.ru/course/mephi/mephi_mo/)
## Clouds
### OCI
### AWS
### GPC
### Azure
## Physics
- [Конструирование: Введение в детали машин](https://openedu.ru/course/mephi/machinery/)
### Nuclear
- [Основы физики ядерных реакторов](https://openedu.ru/course/mephi/mephi_nrpb/)
- [Проект реактора ВВЭР](https://openedu.ru/course/mephi/mephi_wwer/)
- [Управление ядерными знаниями](https://openedu.ru/course/mephi/mephi_013_kmneo/)
## Quantum
- [Введение в квантовые вычисления](https://openedu.ru/course/msu/QUANTUMCOMPUTING/)

